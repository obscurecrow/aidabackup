# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 12:07:30 2018

@author: Byakuya
"""

#app utile anche in strutture complesse (ministeri, universitÃ , ecc)

import json
import math
from myClassifier import MyClassifier

class AIDAwebserver:
    
    classifier = 0;
    monuments = 0;
    
    def __init__(self):        
        #leggiamo i documenti dal json; creiamo quindi la nostra lista di descrizioni dei monumenti e dei nomi degli stessi
        monumentFile = open('./res/monumenti.json', encoding='utf-8')
        json_data = monumentFile.read()
        data = json.loads(json_data)
        monumentFile.close()
        self.monuments = data['monumenti']
        self.classifier = MyClassifier('./res/domande.csv')
        print("PARTITO")
        
    def findAnswer(self,myId,question):
        tag = self.classifier.classifyQuestion(question)
        myId = int(myId)
        if myId < 100:
            try:
                answer = self.monuments[myId][tag[0]]
            except KeyError:
                answer = "Non ho una risposta, mi spiace!\n"
        else:
            try:
                answer = self.monuments[math.floor(myId/100)-1]["Children"][myId-100][tag[0]]
            except KeyError:
                try:
                    answer = self.monuments[math.floor(myId/100)-1][tag[0]]
                except KeyError:
                    answer = "Non ho una risposta, mi spiace!\n"
        return answer