import cherrypy
import json
from AIDA_WebServer import AIDAwebserver

def CORS():
    """Allow web apps not on the same server to use our API
    """
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*" 
    cherrypy.response.headers["Access-Control-Allow-Headers"] = (
        "content-type, Authorization, X-Requested-With"
    )
    
    cherrypy.response.headers["Access-Control-Allow-Methods"] = (
        'GET, POST, PUT, DELETE, OPTIONS'
    )

@cherrypy.expose
class AIDA_WebService(object):
    aida = []
    def __init__(self):
        self.aida = AIDAwebserver()    
    
    @cherrypy.tools.accept(media='*/*')
    
    def POST(self, myID, question):
        answer = self.aida.findAnswer(myID, question)
        return json.dumps(answer)
    
    def OPTIONS(self, *args, **kwargs):
        return "GET, POST"


if __name__ == '__main__':
    
    cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS);
    
    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        }
    }
    cherrypy.config.update({
        #'server.socket_host': '212.189.129.236',
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 9800,
        'tools.CORS.on': True
    })
    cherrypy.quickstart(AIDA_WebService(), '/', conf)